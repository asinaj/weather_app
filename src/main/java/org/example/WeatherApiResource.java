package org.example;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&hourly=temperature_2m")
public interface WeatherApiResource {

    @GET
    public Object getWeatherForecast();
}
