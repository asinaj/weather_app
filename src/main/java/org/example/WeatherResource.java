package org.example;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/api/weather-app/data")
public interface WeatherResource {

    @GET
    public WeatherData getData();
}
