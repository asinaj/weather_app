package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;

public class WeatherApiResourceImpl implements WeatherResource{

    private final WeatherApiResource api;

    public WeatherApiResourceImpl(WeatherApiResource api) {
        this.api = api;
    }

    public WeatherData getData(){
        ObjectMapper mapper= new ObjectMapper();
        return mapper.convertValue(api.getWeatherForecast(), WeatherData.class);
    }
}
