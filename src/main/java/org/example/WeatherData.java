package org.example;

public class WeatherData {
    private final double latitude;
    private final double longitude;
    private final String timezone;

    public WeatherData(double latitude, double longitude, String timezone) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timezone = timezone;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getTimezone() {
        return timezone;
    }
}
